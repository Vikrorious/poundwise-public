#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "FauxPasAnnotations 2.h"
#import "FauxPasAnnotations.h"
#import "STDSAlreadyInitializedException 2.h"
#import "STDSAlreadyInitializedException.h"
#import "STDSAuthenticationRequestParameters 2.h"
#import "STDSAuthenticationRequestParameters.h"
#import "STDSAuthenticationResponse 2.h"
#import "STDSAuthenticationResponse.h"
#import "STDSButtonCustomization 2.h"
#import "STDSButtonCustomization.h"
#import "STDSChallengeParameters 2.h"
#import "STDSChallengeParameters.h"
#import "STDSChallengeStatusReceiver 2.h"
#import "STDSChallengeStatusReceiver.h"
#import "STDSCompletionEvent 2.h"
#import "STDSCompletionEvent.h"
#import "STDSConfigParameters 2.h"
#import "STDSConfigParameters.h"
#import "STDSCustomization 2.h"
#import "STDSCustomization.h"
#import "STDSErrorMessage 2.h"
#import "STDSErrorMessage.h"
#import "STDSException 2.h"
#import "STDSException.h"
#import "STDSFooterCustomization 2.h"
#import "STDSFooterCustomization.h"
#import "STDSInvalidInputException 2.h"
#import "STDSInvalidInputException.h"
#import "STDSJSONDecodable 2.h"
#import "STDSJSONDecodable.h"
#import "STDSJSONEncodable 2.h"
#import "STDSJSONEncodable.h"
#import "STDSJSONEncoder 2.h"
#import "STDSJSONEncoder.h"
#import "STDSLabelCustomization 2.h"
#import "STDSLabelCustomization.h"
#import "STDSNavigationBarCustomization 2.h"
#import "STDSNavigationBarCustomization.h"
#import "STDSNotInitializedException 2.h"
#import "STDSNotInitializedException.h"
#import "STDSProtocolErrorEvent 2.h"
#import "STDSProtocolErrorEvent.h"
#import "STDSRuntimeErrorEvent 2.h"
#import "STDSRuntimeErrorEvent.h"
#import "STDSRuntimeException 2.h"
#import "STDSRuntimeException.h"
#import "STDSSelectionCustomization 2.h"
#import "STDSSelectionCustomization.h"
#import "STDSStripe3DS2Error 2.h"
#import "STDSStripe3DS2Error.h"
#import "STDSTextFieldCustomization 2.h"
#import "STDSTextFieldCustomization.h"
#import "STDSThreeDS2Service 2.h"
#import "STDSThreeDS2Service.h"
#import "STDSThreeDSProtocolVersion 2.h"
#import "STDSThreeDSProtocolVersion.h"
#import "STDSTransaction 2.h"
#import "STDSTransaction.h"
#import "STDSUICustomization 2.h"
#import "STDSUICustomization.h"
#import "STDSWarning 2.h"
#import "STDSWarning.h"
#import "STPAddCardViewController 2.h"
#import "STPAddCardViewController.h"
#import "STPAddress 2.h"
#import "STPAddress.h"
#import "STPAPIClient 2.h"
#import "STPAPIClient+ApplePay 2.h"
#import "STPAPIClient+ApplePay.h"
#import "STPAPIClient.h"
#import "STPAPIResponseDecodable 2.h"
#import "STPAPIResponseDecodable.h"
#import "STPAppInfo 2.h"
#import "STPAppInfo.h"
#import "STPApplePayPaymentOption 2.h"
#import "STPApplePayPaymentOption.h"
#import "STPAuthenticationContext 2.h"
#import "STPAuthenticationContext.h"
#import "STPBackendAPIAdapter 2.h"
#import "STPBackendAPIAdapter.h"
#import "STPBankAccount 2.h"
#import "STPBankAccount.h"
#import "STPBankAccountParams 2.h"
#import "STPBankAccountParams.h"
#import "STPBankSelectionViewController 2.h"
#import "STPBankSelectionViewController.h"
#import "STPBlocks 2.h"
#import "STPBlocks.h"
#import "STPCard 2.h"
#import "STPCard.h"
#import "STPCardBrand 2.h"
#import "STPCardBrand.h"
#import "STPCardParams 2.h"
#import "STPCardParams.h"
#import "STPCardValidationState 2.h"
#import "STPCardValidationState.h"
#import "STPCardValidator 2.h"
#import "STPCardValidator.h"
#import "STPConnectAccountAddress 2.h"
#import "STPConnectAccountAddress.h"
#import "STPConnectAccountCompanyParams 2.h"
#import "STPConnectAccountCompanyParams.h"
#import "STPConnectAccountIndividualParams 2.h"
#import "STPConnectAccountIndividualParams.h"
#import "STPConnectAccountParams 2.h"
#import "STPConnectAccountParams.h"
#import "STPCoreScrollViewController 2.h"
#import "STPCoreScrollViewController.h"
#import "STPCoreTableViewController 2.h"
#import "STPCoreTableViewController.h"
#import "STPCoreViewController 2.h"
#import "STPCoreViewController.h"
#import "STPCustomer 2.h"
#import "STPCustomer.h"
#import "STPCustomerContext 2.h"
#import "STPCustomerContext.h"
#import "STPEphemeralKeyProvider 2.h"
#import "STPEphemeralKeyProvider.h"
#import "STPFakeAddPaymentPassViewController 2.h"
#import "STPFakeAddPaymentPassViewController.h"
#import "STPFile 2.h"
#import "STPFile.h"
#import "STPFormEncodable 2.h"
#import "STPFormEncodable.h"
#import "STPFPXBankBrand 2.h"
#import "STPFPXBankBrand.h"
#import "STPImageLibrary 2.h"
#import "STPImageLibrary.h"
#import "STPIntentAction 2.h"
#import "STPIntentAction.h"
#import "STPIntentActionRedirectToURL 2.h"
#import "STPIntentActionRedirectToURL.h"
#import "STPIssuingCardPin 2.h"
#import "STPIssuingCardPin.h"
#import "STPMandateCustomerAcceptanceParams 2.h"
#import "STPMandateCustomerAcceptanceParams.h"
#import "STPMandateDataParams 2.h"
#import "STPMandateDataParams.h"
#import "STPMandateOnlineParams 2.h"
#import "STPMandateOnlineParams.h"
#import "STPPaymentActivityIndicatorView 2.h"
#import "STPPaymentActivityIndicatorView.h"
#import "STPPaymentCardTextField 2.h"
#import "STPPaymentCardTextField.h"
#import "STPPaymentConfiguration 2.h"
#import "STPPaymentConfiguration.h"
#import "STPPaymentContext 2.h"
#import "STPPaymentContext.h"
#import "STPPaymentHandler 2.h"
#import "STPPaymentHandler.h"
#import "STPPaymentIntent 2.h"
#import "STPPaymentIntent.h"
#import "STPPaymentIntentAction 2.h"
#import "STPPaymentIntentAction.h"
#import "STPPaymentIntentActionRedirectToURL 2.h"
#import "STPPaymentIntentActionRedirectToURL.h"
#import "STPPaymentIntentEnums 2.h"
#import "STPPaymentIntentEnums.h"
#import "STPPaymentIntentLastPaymentError 2.h"
#import "STPPaymentIntentLastPaymentError.h"
#import "STPPaymentIntentParams 2.h"
#import "STPPaymentIntentParams.h"
#import "STPPaymentIntentSourceAction 2.h"
#import "STPPaymentIntentSourceAction.h"
#import "STPPaymentIntentSourceActionAuthorizeWithURL 2.h"
#import "STPPaymentIntentSourceActionAuthorizeWithURL.h"
#import "STPPaymentMethod 2.h"
#import "STPPaymentMethod.h"
#import "STPPaymentMethodAddress 2.h"
#import "STPPaymentMethodAddress.h"
#import "STPPaymentMethodBillingDetails 2.h"
#import "STPPaymentMethodBillingDetails.h"
#import "STPPaymentMethodCard 2.h"
#import "STPPaymentMethodCard.h"
#import "STPPaymentMethodCardChecks 2.h"
#import "STPPaymentMethodCardChecks.h"
#import "STPPaymentMethodCardParams 2.h"
#import "STPPaymentMethodCardParams.h"
#import "STPPaymentMethodCardPresent 2.h"
#import "STPPaymentMethodCardPresent.h"
#import "STPPaymentMethodCardWallet 2.h"
#import "STPPaymentMethodCardWallet.h"
#import "STPPaymentMethodCardWalletMasterpass 2.h"
#import "STPPaymentMethodCardWalletMasterpass.h"
#import "STPPaymentMethodCardWalletVisaCheckout 2.h"
#import "STPPaymentMethodCardWalletVisaCheckout.h"
#import "STPPaymentMethodEnums 2.h"
#import "STPPaymentMethodEnums.h"
#import "STPPaymentMethodFPX 2.h"
#import "STPPaymentMethodFPX.h"
#import "STPPaymentMethodFPXParams 2.h"
#import "STPPaymentMethodFPXParams.h"
#import "STPPaymentMethodiDEAL 2.h"
#import "STPPaymentMethodiDEAL.h"
#import "STPPaymentMethodiDEALParams 2.h"
#import "STPPaymentMethodiDEALParams.h"
#import "STPPaymentMethodParams 2.h"
#import "STPPaymentMethodParams.h"
#import "STPPaymentMethodSEPADebit 2.h"
#import "STPPaymentMethodSEPADebit.h"
#import "STPPaymentMethodSEPADebitParams 2.h"
#import "STPPaymentMethodSEPADebitParams.h"
#import "STPPaymentMethodThreeDSecureUsage 2.h"
#import "STPPaymentMethodThreeDSecureUsage.h"
#import "STPPaymentOption 2.h"
#import "STPPaymentOption.h"
#import "STPPaymentOptionsViewController 2.h"
#import "STPPaymentOptionsViewController.h"
#import "STPPaymentResult 2.h"
#import "STPPaymentResult.h"
#import "STPPinManagementService 2.h"
#import "STPPinManagementService.h"
#import "STPPushProvisioningContext 2.h"
#import "STPPushProvisioningContext.h"
#import "STPPushProvisioningDetailsParams 2.h"
#import "STPPushProvisioningDetailsParams.h"
#import "STPRedirectContext 2.h"
#import "STPRedirectContext.h"
#import "STPSetupIntent 2.h"
#import "STPSetupIntent.h"
#import "STPSetupIntentConfirmParams 2.h"
#import "STPSetupIntentConfirmParams.h"
#import "STPSetupIntentEnums 2.h"
#import "STPSetupIntentEnums.h"
#import "STPSetupIntentLastSetupError 2.h"
#import "STPSetupIntentLastSetupError.h"
#import "STPShippingAddressViewController 2.h"
#import "STPShippingAddressViewController.h"
#import "STPSource 2.h"
#import "STPSource.h"
#import "STPSourceCardDetails 2.h"
#import "STPSourceCardDetails.h"
#import "STPSourceEnums 2.h"
#import "STPSourceEnums.h"
#import "STPSourceOwner 2.h"
#import "STPSourceOwner.h"
#import "STPSourceParams 2.h"
#import "STPSourceParams.h"
#import "STPSourceProtocol 2.h"
#import "STPSourceProtocol.h"
#import "STPSourceReceiver 2.h"
#import "STPSourceReceiver.h"
#import "STPSourceRedirect 2.h"
#import "STPSourceRedirect.h"
#import "STPSourceSEPADebitDetails 2.h"
#import "STPSourceSEPADebitDetails.h"
#import "STPSourceVerification 2.h"
#import "STPSourceVerification.h"
#import "STPSourceWeChatPayDetails 2.h"
#import "STPSourceWeChatPayDetails.h"
#import "STPTheme 2.h"
#import "STPTheme.h"
#import "STPThreeDSButtonCustomization 2.h"
#import "STPThreeDSButtonCustomization.h"
#import "STPThreeDSCustomizationSettings 2.h"
#import "STPThreeDSCustomizationSettings.h"
#import "STPThreeDSFooterCustomization 2.h"
#import "STPThreeDSFooterCustomization.h"
#import "STPThreeDSLabelCustomization 2.h"
#import "STPThreeDSLabelCustomization.h"
#import "STPThreeDSNavigationBarCustomization 2.h"
#import "STPThreeDSNavigationBarCustomization.h"
#import "STPThreeDSSelectionCustomization 2.h"
#import "STPThreeDSSelectionCustomization.h"
#import "STPThreeDSTextFieldCustomization 2.h"
#import "STPThreeDSTextFieldCustomization.h"
#import "STPThreeDSUICustomization 2.h"
#import "STPThreeDSUICustomization.h"
#import "STPToken 2.h"
#import "STPToken.h"
#import "STPUserInformation 2.h"
#import "STPUserInformation.h"
#import "Stripe 2.h"
#import "Stripe.h"
#import "Stripe3DS2 2.h"
#import "Stripe3DS2.h"
#import "StripeError 2.h"
#import "StripeError.h"
#import "UINavigationBar+Stripe_Theme 2.h"
#import "UINavigationBar+Stripe_Theme.h"

FOUNDATION_EXPORT double StripeVersionNumber;
FOUNDATION_EXPORT const unsigned char StripeVersionString[];

