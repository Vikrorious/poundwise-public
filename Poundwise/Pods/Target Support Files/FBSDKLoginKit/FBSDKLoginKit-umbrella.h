#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "FBSDKDeviceLoginCodeInfo 3.h"
#import "FBSDKDeviceLoginCodeInfo.h"
#import "FBSDKDeviceLoginManager 3.h"
#import "FBSDKDeviceLoginManager.h"
#import "FBSDKDeviceLoginManagerResult 3.h"
#import "FBSDKDeviceLoginManagerResult.h"
#import "FBSDKLoginButton 3.h"
#import "FBSDKLoginButton.h"
#import "FBSDKLoginConstants 3.h"
#import "FBSDKLoginConstants.h"
#import "FBSDKLoginKit 3.h"
#import "FBSDKLoginKit.h"
#import "FBSDKLoginManager 3.h"
#import "FBSDKLoginManager.h"
#import "FBSDKLoginManagerLoginResult 3.h"
#import "FBSDKLoginManagerLoginResult.h"
#import "FBSDKLoginTooltipView 3.h"
#import "FBSDKLoginTooltipView.h"
#import "FBSDKTooltipView 3.h"
#import "FBSDKTooltipView.h"

FOUNDATION_EXPORT double FBSDKLoginKitVersionNumber;
FOUNDATION_EXPORT const unsigned char FBSDKLoginKitVersionString[];

