//
//  AppDelegate.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 11/23/18.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        //resetGoalVC()
        if (UserDefaults.standard.string(forKey: "UserID") == nil){
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = UINavigationController(rootViewController: ATCClassicLandingScreenViewController(nibName: "ATCClassicLandingScreenViewController", bundle: nil))
            window?.makeKeyAndVisible()
            // Override point for customization after application launch.
        }
        else{
            var ref = Database.database().reference()
            let userUID = UserDefaults.standard.string(forKey: "UserID")
            ref.child("users").child(userUID!).child("goals").observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                //print(snapshot.value)
                let children = snapshot.children
                while let goal = children.nextObject() as? DataSnapshot, let value = goal.value {
                    //self.yourArray.append(value as! [String: Any])
                    //print(rest.value)
                    print(goal.childSnapshot(forPath: "active").value!)
                    let isActive = goal.childSnapshot(forPath: "active").value! as! Int
                    if (isActive == 1){
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        let newGoalVC = sb.instantiateViewController(withIdentifier: "goal1") as! GoalViewController
                        self.initNewGoalVC(vc: newGoalVC, goal: goal)
                        
                        addNewGoalVC(newGoal: newGoalVC)
                    }
                }

                // ...
              }) { (error) in
                print(error.localizedDescription)
            }
            
        }

        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Poundwise")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    // MARK: - Our own functions
    func initNewGoalVC(vc: GoalViewController, goal: DataSnapshot){
        vc.dueDateString = goal.childSnapshot(forPath: "date").value! as? String
        vc.goalID = goal.childSnapshot(forPath: "goalID").value! as? String
        vc.targetReps = goal.childSnapshot(forPath: "targetReps").value! as? Int
        vc.goalType = goal.childSnapshot(forPath: "type").value! as? Int
        vc.wager = goal.childSnapshot(forPath: "wager").value! as? Int
    }

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

