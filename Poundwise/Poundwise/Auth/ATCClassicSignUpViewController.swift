//
//  ATCClassicSignUpViewController.swift
//  DashboardApp
//
//  Created by Florian Marcu on 8/10/18.
//  Copyright © 2018 Instamobile. All rights reserved.
//

import UIKit
import Firebase

class ATCClassicSignUpViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var firstNameTextField: ATCTextField!
    @IBOutlet var lastNameTextField: ATCTextField!
    @IBOutlet var passwordTextField: ATCTextField!
    @IBOutlet var emailTextField: ATCTextField!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var signUpButton: UIButton!

    
    private let tintColor = UIColor(hexString: "#ff5a66")
    private let backgroundColor: UIColor = HelperDarkMode.mainThemeBackgroundColor
    private let textFieldColor = UIColor(hexString: "#B0B3C6")
    
    private let textFieldBorderColor = UIColor(hexString: "#B0B3C6")

    private let titleFont = UIFont.systemFont(ofSize: 30, weight: .heavy)
    private let textFieldFont = UIFont.systemFont(ofSize: 16)
    private let buttonFont = UIFont.boldSystemFont(ofSize: 20)
    
    var ref = Database.database().reference()
    var sb = UIStoryboard()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = backgroundColor
        let color = UIColor(hexString: "#ff5a66")
        backButton.tintColor = color
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)

        titleLabel.font = titleFont
        titleLabel.text = "Sign Up"
        titleLabel.textColor = tintColor
        

        firstNameTextField.configure(color: UIColor(hexString: "#ff5a66"),
                                font: textFieldFont,
                                cornerRadius: 40/2,
                                borderColor: textFieldBorderColor,
            backgroundColor: backgroundColor,
            borderWidth: 1.0)
        firstNameTextField.placeholder = "First Name"
        firstNameTextField.clipsToBounds = true

        emailTextField.configure(color: UIColor(hexString: "#ff5a66"),
                                 font: textFieldFont,
                                 cornerRadius: 40/2,
                                 borderColor: textFieldBorderColor,
            backgroundColor: backgroundColor,
            borderWidth: 1.0)
        emailTextField.placeholder = "E-mail Address"
        emailTextField.clipsToBounds = true

        lastNameTextField.configure(color: UIColor(hexString: "#ff5a66"),
                                       font: textFieldFont,
                                       cornerRadius: 40/2,
                                       borderColor: textFieldBorderColor,
            backgroundColor: backgroundColor,
            borderWidth: 1.0)
        lastNameTextField.placeholder = "Last Name"
        lastNameTextField.clipsToBounds = true

        passwordTextField.configure(color: UIColor(hexString: "#ff5a66"),
                                    font: textFieldFont,
                                    cornerRadius: 40/2,
                                    borderColor: textFieldBorderColor,
                                    backgroundColor: backgroundColor,
                                    borderWidth: 1.0)
            passwordTextField.placeholder = "Password"
            passwordTextField.isSecureTextEntry = true
            passwordTextField.clipsToBounds = true

        signUpButton.setTitle("Create Account", for: .normal)
        //signUpButton.addTarget(self, action: #selector(didTapSignUpButton), for: .touchUpInside)
        signUpButton.configure(color: backgroundColor,
                               font: buttonFont,
                               cornerRadius: 40/2,
                               backgroundColor: UIColor(hexString: "#ff5a66"))

        self.hideKeyboardWhenTappedAround()
    }
    
    func createUserBranch(){
        let user = Auth.auth().currentUser
        let userUID = user?.uid
        //setUID(uid: userUID!)
        let autoIDkey = self.ref.child("users").childByAutoId().key
        self.ref.child("users").child(userUID!).child("basicInfo").child("uid").setValue(user?.uid)
        self.ref.child("users").child(userUID!).child("basicInfo").child("email").setValue(emailTextField.text)
        self.ref.child("users").child(userUID!).child("basicInfo").child("firstName").setValue(firstNameTextField?.text)
        self.ref.child("users").child(userUID!).child("basicInfo").child("lastName").setValue(lastNameTextField?.text)
        
        
    }
    

    
    @IBAction func hitSignUpButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { authResult, error in
          if error != nil{
              print(error!.localizedDescription)
              if let errCode = AuthErrorCode(rawValue: error!._code) {
                  
                  switch errCode {
                  case .invalidEmail:
                      print("invalid email")
                      let alertController = UIAlertController(title: "Whoops!", message:
                          "That email is invalid!", preferredStyle: UIAlertControllerStyle.alert)
                      alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                      self.present(alertController, animated: true, completion: nil)
                  case .emailAlreadyInUse:
                      print("in use")
                      let alertController = UIAlertController(title: "Whoops!", message:
                          "That email is already in use!", preferredStyle: UIAlertControllerStyle.alert)
                      alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                      self.present(alertController, animated: true, completion: nil)
                  case .internalError:
                      print("an internal error has occured")
                      let alertController = UIAlertController(title: "An Internal Error Has Occured", message:
                          "See if there's anything wrong with your device or internet.", preferredStyle: UIAlertControllerStyle.alert)
                      alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                      self.present(alertController, animated: true, completion: nil)
                  default:
                      print("Create User Error: \(error!)")
                      let alertController = UIAlertController(title: "Uh oh!", message:
                          "Something went wrong. Try again.", preferredStyle: UIAlertControllerStyle.alert)
                      alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                      self.present(alertController, animated: true, completion: nil)
                  }
              }
              return
          }
          else{
              print("successful signup")
              print(authResult?.additionalUserInfo)
              
              //let viewController: UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "postSignupScreenID") )!

              //[self.present(viewController, animated: true, completion: nil)] as [Any];
              //updatePresentID(presentid: "postSignupScreenID")
              self.createUserBranch()
              let user = Auth.auth().currentUser
              UserDefaults.standard.set(user?.uid, forKey: "UserID")
              self.sb = UIStoryboard(name: "Main", bundle: nil)
              let homeScreen = self.sb.instantiateViewController(withIdentifier: "homeVC")
              homeScreen.modalPresentationStyle = .fullScreen
              self.present(homeScreen, animated: true, completion: nil)
              
          }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    @objc func didTapBackButton() {
        self.navigationController?.popViewController(animated: true)
    }


    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
}
