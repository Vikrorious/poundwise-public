//
//  ATCClassicLoginScreenViewController.swift
//  DashboardApp
//
//  Created by Florian Marcu on 8/9/18.
//  Copyright © 2018 Instamobile. All rights reserved.
//

import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import Firebase
import UIKit

class ATCClassicLoginScreenViewController: UIViewController {
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var passwordTextField: ATCTextField!
  @IBOutlet var contactPointTextField: ATCTextField!
  @IBOutlet var loginButton: UIButton!
  //@IBOutlet var separatorLabel: UILabel!
  //@IBOutlet var facebookButton: UIButton!
  @IBOutlet var backButton: UIButton!
  
  private let backgroundColor = HelperDarkMode.mainThemeBackgroundColor
  private let tintColor = UIColor(hexString: "#ff5a66")
  
  private let titleFont = UIFont.systemFont(ofSize: 30, weight: .heavy)
  private let buttonFont = UIFont.boldSystemFont(ofSize: 20)
  
  private let textFieldFont = UIFont.systemFont(ofSize: 16)
  //private let textFieldColor = UIColor(hexString: "#B0B3C6")
  private let textFieldBorderColor = UIColor(hexString: "#B0B3C6")
  
  private let separatorFont = UIFont.boldSystemFont(ofSize: 14)
  private let separatorTextColor = UIColor(hexString: "#464646")
    
    var sb = UIStoryboard()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = backgroundColor
    backButton.setImage(UIImage.localImage("arrow-back-icon", template: true), for: .normal)
    backButton.tintColor = UIColor(hexString: "#ff5a66")
    backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
    
    titleLabel.font = titleFont
    titleLabel.text = "Log In"
    titleLabel.textColor = tintColor
    
    contactPointTextField.configure(font: textFieldFont,
                                    cornerRadius: 55/2,
                                    borderColor: textFieldBorderColor,
                                    backgroundColor: backgroundColor,
                                    borderWidth: 1.0)
    contactPointTextField.placeholder = "E-mail"
    contactPointTextField.textContentType = .emailAddress
    contactPointTextField.clipsToBounds = true
    
    passwordTextField.configure(font: textFieldFont,
                                cornerRadius: 55/2,
                                borderColor: textFieldBorderColor,
                                backgroundColor: backgroundColor,
                                borderWidth: 1.0)
    passwordTextField.placeholder = "Password"
    passwordTextField.isSecureTextEntry = true
    passwordTextField.textContentType = .emailAddress
    passwordTextField.clipsToBounds = true
    
    //separatorLabel.font = separatorFont
    //separatorLabel.textColor = separatorTextColor
    //separatorLabel.text = "OR"
    
    loginButton.setTitle("Log In", for: .normal)
    //loginButton.addTarget(self, action: #selector(didTapLoginButton), for: .touchUpInside)
    loginButton.configure(color: backgroundColor,
                          font: buttonFont,
                          cornerRadius: 55/2,
                          backgroundColor: tintColor)
    
    //facebookButton.setTitle("Facebook Login", for: .normal)
    //facebookButton.addTarget(self, action: #selector(didTapFacebookButton), for: .touchUpInside)
    //facebookButton.configure(color: backgroundColor,
                             //font: buttonFont,
                             //cornerRadius: 55/2,
                             //backgroundColor: UIColor(hexString: "#334D92"))
    self.hideKeyboardWhenTappedAround()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
  @objc func didTapBackButton() {
    self.navigationController?.popViewController(animated: true)
  }
    
    @IBAction func hitLoginButton(_ sender: Any) {
        Auth.auth().signIn(withEmail: contactPointTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error != nil{
                print(error!.localizedDescription)
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    
                    switch errCode {
                    case .invalidEmail:
                        print("invalid email")
                        let alertController = UIAlertController(title: "Whoops!", message:
                            "That email is invalid!", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    case .emailAlreadyInUse:
                        print("in use")
                        let alertController = UIAlertController(title: "Whoops!", message:
                            "That email is already in use!", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    case .internalError:
                        print("an internal error has occured")
                        let alertController = UIAlertController(title: "An Internal Error Has Occured", message:
                            "See if there's anything wrong with your device or internet.", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    case .wrongPassword:
                        print("wrong password!")
                        let alertController = UIAlertController(title: "Whoops!", message:
                            "That's the wrong password!", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    case .userNotFound:
                        print("no user found")
                        let alertController = UIAlertController(title: "Whoops!", message:
                            "There was no user found with this email!", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    default:
                        print("Create User Error: \(error!)")
                        let alertController = UIAlertController(title: "Uh oh!", message:
                            "Something went wrong. Try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                return
                }
            else{
                
                print("successful login")
                print(user!.additionalUserInfo!)
                print(user!.user.uid)
                UserDefaults.standard.set(user!.user.uid, forKey: "UserID")
                self.sb = UIStoryboard(name: "Main", bundle: nil)
                self.loadUserGoals()
                let homeScreen = self.sb.instantiateViewController(withIdentifier: "homeVC")
                homeScreen.modalPresentationStyle = .fullScreen
                print(getActiveGoalVC())
                self.present(homeScreen, animated: true, completion: nil)
                print(getActiveGoalVC())
                //self.performSegue(withIdentifier: "homeVC", sender: nil)
                //setUID(uid: (user?.user.uid)!)
                //self.performSegue(withIdentifier: "loginSegue", sender: nil)
            }
            }
    }
    
    func initNewGoalVC(vc: GoalViewController, goal: DataSnapshot){
        vc.dueDateString = goal.childSnapshot(forPath: "date").value! as? String
        vc.goalID = goal.childSnapshot(forPath: "goalID").value! as? String
        vc.targetReps = goal.childSnapshot(forPath: "targetReps").value! as? Int
        vc.goalType = goal.childSnapshot(forPath: "type").value! as? Int
        vc.wager = goal.childSnapshot(forPath: "wager").value! as? Int
    }
    
    func loadUserGoals(){
        var ref = Database.database().reference()
        let userUID = UserDefaults.standard.string(forKey: "UserID")
        ref.child("users").child(userUID!).child("goals").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            //print(snapshot.value)
            let children = snapshot.children
            while let goal = children.nextObject() as? DataSnapshot, let value = goal.value {
                //self.yourArray.append(value as! [String: Any])
                //print(rest.value)
                print(goal.childSnapshot(forPath: "active").value!)
                let isActive = goal.childSnapshot(forPath: "active").value! as! Int
                if (isActive == 1){
                    print ("isActive")
                    //self.sb = UIStoryboard(name: "Main", bundle: nil)
                    let newGoalVC = self.sb.instantiateViewController(withIdentifier: "goal1") as! GoalViewController
                    self.initNewGoalVC(vc: newGoalVC, goal: goal)
                    
                    addNewGoalVC(newGoal: newGoalVC)
                    print(getActiveGoalVC())
                }
            }

            // ...
          }) { (error) in
            print(error.localizedDescription)
        }
    }
    
  
  
  
    func display(alertController: UIAlertController) {
      self.present(alertController, animated: true, completion: nil)
    }
    
}
  
  extension ATCClassicLoginScreenViewController {
    
    func showPopup(isSuccess: Bool) {
      let successMessage = "User was sucessfully logged in."
      let errorMessage = "Something went wrong. Please try again"
      let alert = UIAlertController(title: isSuccess ? "Success": "Error", message: isSuccess ? successMessage: errorMessage, preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
}
