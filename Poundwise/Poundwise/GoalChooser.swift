//
//  GoalChooser.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 12/10/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit

class GoalChooser: UIViewController {

    @IBOutlet var goalCollectionView: UICollectionView!
    @IBOutlet var stepsGoal: UIButton!
    @IBOutlet var flightsClimbedGoal: UIButton!
    @IBOutlet var distanceGoal: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func stepsButtonTapped(_ sender: Any) {
        setGoalTypeChosen(goalChosen: 1)
        performSegue(withIdentifier: "chooseToCustomizer", sender: nil)
    }
    
    @IBAction func flightsClimbedButtonPressed(_ sender: Any) {
        setGoalTypeChosen(goalChosen: 2)
        performSegue(withIdentifier: "chooseToCustomizer", sender: nil)
    }
    
    
    @IBAction func distanceButtonPressed(_ sender: Any) {
        setGoalTypeChosen(goalChosen: 3)
        performSegue(withIdentifier: "chooseToCustomizer", sender: nil)
    }
    
    
    /*
    
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
