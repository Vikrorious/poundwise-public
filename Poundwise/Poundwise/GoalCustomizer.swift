//
//  PlanCustomizer.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 1/6/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit
import Firebase

class GoalCustomizer: UIViewController {

    
    @IBOutlet var goalTitle: UILabel!
    @IBOutlet var repsSlider: UISlider!
    @IBOutlet var repsLabel: UILabel!
    @IBOutlet var confidenceDollarLabel: UILabel!
    @IBOutlet var confidenceStepper: UIStepper!
    
    @IBOutlet var planTodayButton: UIButton!
    @IBOutlet var planTomorrowButton: UIButton!
    
    @IBOutlet var confirmView: UIView!
    @IBOutlet var confirmConfidenceLabel: UILabel!
    @IBOutlet var confirmRepsLabel: UILabel!
    @IBOutlet var confirmTodayOrTomorrow: UILabel!
    @IBOutlet var confirmNoButton: UIButton!
    @IBOutlet var confirmYesButton: UIButton!
    @IBOutlet var verbLabel: UILabel!
    
    var goalType = getGoalTypeChosen()
    var repsNumber = Int()
    
    var calendar = Calendar.current
    
    var todayChosen: Bool!
    var dueDate: Date!
    var dueDateString: String!
    
    var ref: DatabaseReference!
    var user = Auth.auth().currentUser


    
    func updateDates(isTodayChosen: Bool){
        let today = Date().localDate()
        let tomorrow = calendar.date(byAdding: .day, value: 1, to: today)
        
        print(today)
        print(tomorrow)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        if (isTodayChosen){
            dueDate = today
            
        }
        else{
            dueDate = tomorrow
        }
        dueDateString = dateFormatter.string(from: dueDate!)
        
    }
    
    func initSlider(min: Float, max: Float, value: Float){
        repsSlider.minimumValue = min
        repsSlider.maximumValue = max
        repsSlider.value = value

    }
    
    func updateRepsNumber(){
        
        if (goalType == 1){
            repsLabel.text = "\(repsNumber) Steps Per Day"
            repsNumber = (Int(round(pow(10.0, repsSlider.value)))/250)*250
        }
        else if (goalType == 2){
            repsLabel.text = "\(repsNumber) Flights Per Day"
            repsNumber = (Int(round(pow(10.0, repsSlider.value)))/5)*5
        }
        else if (goalType == 3){
            repsLabel.text = "\(repsNumber) Miles Per Day"
            repsNumber = Int(round(pow(10.0, repsSlider.value)))
        }
    }
    
    func updateConfirmDialogue(today: Bool){
        confirmConfidenceLabel.text = "$\(Int(confidenceStepper.value)).00"
        
        if (today){
            confirmTodayOrTomorrow.text = "today"
        }
        else{
            confirmTodayOrTomorrow.text = "tomorrow"
        }
        
        if (goalType == 1){
            confirmRepsLabel.text = "\(repsNumber) Steps"
            verbLabel.text = "to taking"
        }
        else if (goalType == 2){
            confirmRepsLabel.text = "\(repsNumber) Flights"
            verbLabel.text = "to climbing"

        }
        else if (goalType == 3){
            confirmRepsLabel.text = "\(repsNumber) Miles"
            verbLabel.text = "to walking/running"

        }
        
    }
    
    func initNewGoalVC(vc: GoalViewController, goalID: UUID){
        vc.dueDateString = dueDateString
        vc.goalID = goalID.uuidString
        vc.targetReps = repsNumber
        vc.goalType = goalType
        vc.wager = Int(confidenceStepper.value)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(goalType)
        
        if (goalType == 1){
            goalTitle.text = "Let's Get Steppin'"
            repsNumber = 10000
            repsLabel.text = "\(repsNumber) Steps Per Day"
            initSlider(min: 3.0, max: 5.0, value: 4.0)
        }
        else if (goalType == 2){
            goalTitle.text = "Let's Get Climbin'"
            repsNumber = 100
            repsLabel.text = "\(repsNumber) Flights Per Day"
            initSlider(min: 1.0, max: 3.0, value: 2.0)
        }
        else if (goalType == 3){
            goalTitle.text = "Let's Get Going"
            repsNumber = 10
            repsLabel.text = "\(repsNumber) Miles Per Day"
            initSlider(min: 0.0, max: 2.0, value: 1.0)
        }
        else{
            goalTitle.text = "Let's Get Movin'"
        }
        confidenceStepper.value = 3
        confidenceDollarLabel.text = "$\(Int(confidenceStepper.value))"
        
        confirmView.isHidden = true
        
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func betSliderChanged(_ sender: Any) {
        updateRepsNumber()
    }
    
    @IBAction func confidenceChanged(_ sender: Any) {
        confidenceDollarLabel.text = "$\(Int(confidenceStepper.value))"
    }
    
    
    
    @IBAction func startToday(_ sender: Any) {
        updateConfirmDialogue(today: true)
        confirmView.isHidden = false
        todayChosen = true
    }
    
    @IBAction func startTomorrow(_ sender: Any) {
        updateConfirmDialogue(today: false)
        confirmView.isHidden = false
        todayChosen = false
    }
    
    @IBAction func negateConfirm(_ sender: Any) {
        confirmView.isHidden = true
    }
    
    @IBAction func affirmConfirm(_ sender: Any) {
        let goalID = UUID()
        if (todayChosen){
            updateDates(isTodayChosen: true)
            
        }
        else{
            updateDates(isTodayChosen: false)
        }
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("date").setValue(dueDateString)
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("goalID").setValue(goalID.uuidString)
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("targetReps").setValue(repsNumber)
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("type").setValue(goalType)
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("wager").setValue(Int(confidenceStepper.value))
        self.ref.child("users").child(UserDefaults.standard.string(forKey: "UserID")!).child("goals").child(goalID.uuidString).child("active").setValue(true)
        
        //let newGoalVC = GoalViewController(date: dueDateString, goalIDParam: goalID.uuidString, targetRepsParam: repsNumber, goalTypeParam: goalType, wagerParam: Int(confidenceStepper.value))
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let newGoalVC = sb.instantiateViewController(withIdentifier: "goal1") as! GoalViewController
        initNewGoalVC(vc: newGoalVC, goalID: goalID)
        
        addNewGoalVC(newGoal: newGoalVC)
        performSegue(withIdentifier: "backToHome", sender: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

