//
//  HomeVC.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 12/31/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit
import HealthKit

class HomeVC: UIViewController {

   let healthStore = HKHealthStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SpeedySloth"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let typesToShare: Set = [
            HKQuantityType.quantityType(forIdentifier: .stepCount)!,
            HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!,
            HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        ]
        
        let typesToRead: Set = [
            HKQuantityType.quantityType(forIdentifier: .stepCount)!,
            HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!,
            HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        ]
      
        healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
            // Handle error
            
        }
    }

}
