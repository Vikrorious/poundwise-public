//
//  OnboardingViewController.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 1/13/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit

class GoalContainerVC: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    enum PageViews: String {
        case dailyPlan
        case weeklyPlan
        case monthlyPlan
        case goal1
        case goalChooser
    }
    
    fileprivate lazy var orderedViewController: [UIViewController] = {
        print("in orderedVC")
        print(getActiveGoalVC())
        return getActiveGoalVC()
        
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return (storyboard?.instantiateViewController(withIdentifier: identifier))!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        orderedViewController = getActiveGoalVC()
        print(orderedViewController)
        if (orderedViewController.count >= 2){
            guard let viewControllerIndex = orderedViewController.index(of: viewController) else { return nil }
            let previousIndex = viewControllerIndex - 1
            guard previousIndex >= 0 else { return orderedViewController.last }
            guard orderedViewController.count > previousIndex else { return nil }
            return orderedViewController[previousIndex]
        }
        else{
            print("in else statement")
            self.delegate = nil
            self.dataSource = nil
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        orderedViewController = getActiveGoalVC()
        print(orderedViewController)
        if (orderedViewController.count >= 2){
            guard let viewControllerIndex = orderedViewController.index(of: viewController) else { return nil }
            let nextIndex = viewControllerIndex + 1
            guard nextIndex < orderedViewController.count else { return orderedViewController.first }
            guard orderedViewController.count > nextIndex else { return nil }
            return orderedViewController[nextIndex]
        }
        else{
            print("in else statement")
            self.delegate = nil
            self.dataSource = nil
            return nil
        }
        
    }
    
    func presentationCount(for: UIPageViewController) -> Int {
        return orderedViewController.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        print("in OVC VDL")
        if let firstVC = orderedViewController.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
