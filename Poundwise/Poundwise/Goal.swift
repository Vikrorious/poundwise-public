//
//  Goal.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 5/25/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit

class Goal: NSObject {
    
    var repsPerDay = Int()
    var gType = goalType.steps
    var gLength = goalLength.daily
    var isTypeA = Bool()
    var gDollarPerDay = Int()
    
    enum goalType {
        case steps
        case calories
        case distance
        case flights
    }
    
    enum goalLength {
        case daily
        case weekly
        case monthly
    }
    
    init(reps: Int, type: goalType, length: goalLength, typeA: Bool, dollarPerDay: Int) {
        repsPerDay = reps
        gType = type
        gLength = length
        isTypeA = typeA
        gDollarPerDay = dollarPerDay
        
    }
    
}
