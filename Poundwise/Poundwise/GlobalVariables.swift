//
//  GlobalVariables.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 12/20/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import Foundation
import UIKit

var goalTypeChosen = Int()
let storyboard = UIStoryboard(name: "Main", bundle: nil)
var activeGoalVC = [storyboard.instantiateViewController(withIdentifier: "goalChooser")]

func setGoalTypeChosen(goalChosen: Int){
    goalTypeChosen = goalChosen
}

func getGoalTypeChosen() -> Int{
    return goalTypeChosen
}

func addNewGoalVC(newGoal: UIViewController){
    activeGoalVC.insert(newGoal, at: 0)
}

func resetGoalVC(){
    activeGoalVC = [storyboard.instantiateViewController(withIdentifier: "goalChooser")]
}

func getActiveGoalVC() -> [UIViewController] {
    return activeGoalVC
}
