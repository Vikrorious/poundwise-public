//
//  DateExtension.swift
//  Poundwise
//
//  Created by Vikram Agrawal on 12/21/19.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import Foundation


extension Date {
    func localDate() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else {return Date()}

        return localDate
    }
}
