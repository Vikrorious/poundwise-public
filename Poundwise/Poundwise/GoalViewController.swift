//
//  DashboardController.swift
//  
//
//  Created by Vikram Agrawal on 12/2/18.
//  Copyright © 2019 TeraThought. All rights reserved.
//

import UIKit

class GoalViewController: UIViewController {

    @IBOutlet weak var fitnessGoalLabel: UILabel!
    @IBOutlet weak var completedVTotalLabel: UILabel!
    @IBOutlet weak var numRemainingLabel: UILabel!
    @IBOutlet weak var dailyConfidenceView: UIVisualEffectView!
    @IBOutlet weak var dailyConfidenceLabel: UILabel!
    @IBOutlet weak var currentPlanView: UIVisualEffectView!
    @IBOutlet weak var currentPlanLabel: UILabel!
    @IBOutlet weak var numDaysLeftLabel: UILabel!
    
    @IBOutlet weak var addNewPlanButton: UIButton!
    @IBOutlet weak var abandonPlanButton: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var dueDateString: String!
    var goalID: String!
    var targetReps: Int!
    var goalType: Int!
    var wager: Int!
    
    var completedReps = 0
    
    /*
    init(date: String, goalIDParam: String, targetRepsParam: Int, goalTypeParam: Int, wagerParam: Int) {
        dueDateString = date
        goalID = goalIDParam
        targetReps = targetRepsParam
        goalType = goalTypeParam
        wager = wagerParam
        super.init(nibName: nil, bundle: nil)

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        dailyConfidenceLabel.text = "Confidence: $\(wager!).00"
        completedVTotalLabel.text = "0/\(targetReps!)"
        numRemainingLabel.text = "Just \(targetReps! - completedReps) to go!"
        
        if (goalType == 1){
            fitnessGoalLabel.text = "STEPS TAKEN"
        }
        else if (goalType == 2){
            fitnessGoalLabel.text = "FLIGHTS CLIMBED"
        }
        else if (goalType == 3){
            fitnessGoalLabel.text = "DISTANCE TRAVELLED"
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
